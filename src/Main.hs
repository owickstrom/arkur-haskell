module Main where

    import Arkur.Core
    import Data.Char
    import Data.Time
    import System.Locale
    import Text.Printf

    ensureEndsWith :: Eq a => [a] -> a -> [a]
    ensureEndsWith xs c = if last xs == c then xs else xs ++ [c]

    showRelative :: Person -> Maybe Person -> String -> String
    showRelative person relative rName =
        let name = fullName person
        in case relative of
            Nothing -> printf "%s has no %s.\n" name rName
            Just p -> printf "%s %s %s was born %s.\n" 
                (ensureEndsWith name 's') 
                rName 
                (fullName p) 
                (showPersonBirthday p)

    showRelatives :: Person -> [((Person -> Maybe Person), String)] -> String
    showRelatives person relationTypes = foldl1 (++) $ map showRelativeByType relationTypes
        where showRelativeByType (rf, rName) = showRelative person (rf person) rName


    showPersonBirthday :: Person -> String
    showPersonBirthday person = formatTime defaultTimeLocale "%F %X" $ birthday person

    showPerson :: PersonRepository r => r -> Person -> String
    showPerson repo person =
        printf "%s was born %s.\n" (fullName person) (showPersonBirthday person) ++ parentsText
        where 
            father = getFatherOf repo
            mother = getMotherOf repo
            fathersFather p = father p >>= father
            fathersMother p = father p >>= mother
            mothersFather p = mother p >>= father
            mothersMother p = mother p >>= mother
            parentsText = showRelatives person [(fathersFather, "grand father"),
                                                (fathersMother, "grand mother"),
                                                (father, "father"),
                                                (mothersFather, "grand father"),
                                                (mothersMother, "grand mother"),
                                                (mother, "mother")] 

    main :: IO ()
    main = do
        time <- getZonedTime
        let grandParents1 = [createPerson "Hans" "Jonsson" Male time, 
                             createPerson "Berta" "Jonsson" Female time]
            grandParents2 = [createPerson "Bengt" "Karlsson" Male time, 
                             createPerson "Greta" "Karlsson" Female time]
            grandParents = grandParents1 ++ grandParents2

            parent1 = createPerson "Karl" "Karlsson" Male time
            parent2 = createPerson "Maja" "Karlsson" Female time
            parents = [parent1, parent2]

            children = [createPerson "Nils" "Karlsson" Male time, 
                        createPerson "Lisa" "Karlsson" Female time]

            persons = grandParents ++ parents ++ children
            relations = 
                createParentRelations grandParents1 [parent1] ++
                createParentRelations grandParents2 [parent2] ++
                createParentRelations parents children
            repo = ListPersonRepository persons relations

        mapM_ (putStrLn . showPerson repo) children