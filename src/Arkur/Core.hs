module Arkur.Core where

    import Data.List
    import Data.Char
    import Data.Maybe
    import Data.Time

    -- | A 'Gender' which can be either 'Male' or 'Female'.
    data Gender = Male | Female deriving (Eq, Show)

    -- | A 'Person' is a unique individual within a single tenant.
    data Person = Person { identity :: String
                         , firstName :: String
                         , lastName :: String 
                         , gender :: Gender
                         , birthday :: ZonedTime }
                         deriving (Show)

    -- | A relation between a parent and its child, using their identities.
    type ParentRelation = (String, String)
    -- | A list of 'ParentRelation's.
    type ParentRelations = [ParentRelation]

    -- | A repository of 'Person's and their relations.
    class PersonRepository r where
        -- | Gets a 'Maybe' 'Person' by identity.
        getPerson :: r -> String -> Maybe Person
        -- | Gets all 'ParentRelations'.
        getParentRelations :: r -> ParentRelations

    -- | A list-based 'PersonRepository'.
    data ListPersonRepository = ListPersonRepository [Person] ParentRelations 
        deriving (Show)

    instance PersonRepository ListPersonRepository where
        getPerson (ListPersonRepository ps _) i = 
            find (\p -> identity p == i) ps
        getParentRelations (ListPersonRepository _ r) = r

    instance Eq Person where
        p1 == p2 = (identity p1) == (identity p2)
        p1 /= p2 = not (p1 == p2)

    -- | Returns the full name of a 'Person'.
    fullName :: Person -> String
    fullName p = firstName p ++ " " ++ lastName p

    -- | Selects 'ParentRelation's where the given 'Person' is the child.
    isPersonChildInRelation :: Person -> ParentRelation -> Bool
    isPersonChildInRelation p r = identity p == snd r

    -- | Selects 'Person's who has the given 'Gender'.
    isOfGender :: Gender -> Person -> Bool
    isOfGender g p = g == gender p

    getParentsFrom :: PersonRepository r => r -> ParentRelations -> [Person]
    getParentsFrom repo = mapMaybe (getPerson repo . fst)

    getParentOf :: PersonRepository r => Gender -> r -> Person -> Maybe Person
    getParentOf g repo p =
        let isChildInRelation = isPersonChildInRelation p
            allParentRelations = getParentRelations repo
            parentRelations = filter isChildInRelation allParentRelations
            parentsOfGender = filter (isOfGender g) $ getParentsFrom repo parentRelations
        in listToMaybe parentsOfGender

    getFatherOf :: PersonRepository r => r -> Person -> Maybe Person
    getFatherOf = getParentOf Male

    getMotherOf :: PersonRepository r => r -> Person -> Maybe Person
    getMotherOf = getParentOf Female

    createParentRelations :: [Person] -> [Person] -> ParentRelations
    createParentRelations parents children = [(identity p, identity c) | p <- parents, c <- children]

    createPerson :: String -> String -> Gender -> ZonedTime -> Person
    createPerson fname lname = Person (map toLower (fname ++ "." ++ lname)) fname lname

