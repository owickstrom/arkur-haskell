module Arkur.Time where

    import Data.Time
    import Data.Fixed

    dateTime :: Integer -> Int -> Int -> Int -> Int -> Pico -> TimeZone -> ZonedTime
    dateTime year month day hour minute second tz  = 
        utcToZonedTime tz $ 
            UTCTime 
                (fromGregorian year month day) 
                (timeOfDayToTime $ TimeOfDay hour minute second)